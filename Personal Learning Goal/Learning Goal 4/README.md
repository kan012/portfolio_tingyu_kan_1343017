# Learning Goal 4: Social impact of traffic data

## 1. The Background:

In my initial personal learning goals, my aim was to comprehend the influence of traffic flow and social media data on governmental decisions or policies. Given the increasing significance of real-time mapping tools like Google Maps and Apple Maps in our daily lives, both individuals and the government now have the capability to promptly gauge traffic conditions, particularly the flow and congestion during peak hours. Consequently, my interest lies in exploring how these mapping platforms have already impacted, or have the potential to impact, government decision-making processes.


## 2. Methodology and data source used:

To understand the utilization of traffic-related data in governmental decision-making, I conducted a exploration on Google Scholar, employing revelent keywords such as 'traffic data,' 'real-time traffic flow,' 'Google Maps,' 'government,' and 'policy.', etc. Having summarize through relevant literatures, my emphasis was on delineating how the government harnesses this data to enhance urban traffic conditions, particularly through the formulation of policies mostly focusing on congestion during peak hours.
 

## 3. Results:

As the concept of smart cities takes center stage in shaping future urban operations, numerous instances highlight the significant role of traffic data at both the societal and governmental levels. For instance, a study focused on Budapest utilized Google Maps to analyze the city's traffic dynamics, delving into temporal and spatial congestion at critical intersections and roads. The findings not only provided insights into traffic issues but also offered suggestions for improving the overall traffic situation (Baji, 2018). In another case, a rerouting algorithm model, constructed using web-based platforms like Google Maps, not only alleviated congestion during peak hours but also effectively curtailed CO2 emissions and fuel consumption (Tseng & Ferng, 2021). A parallel approach was presented by Zubairia et al. (Zubairi et al., 2022). 

A compelling case showcasing the practical impact of traffic data analysis on government decision-making occurred in Bangladesh during the COVID-19 epidemic. The government implemented various measures, including general holidays, closure of educational institutions, deployment of forces, restrictions on religious gatherings, and the use of traffic data, etc., to manage crowd gatherings. By assessing the amount of traffic in the city as a proxy for gathering levels, the Bangladeshi government examined the effectiveness of these policies. In this instance, traffic data emerged as a vital tool for the government in the policymaking process(Zafri et al., 2021). 

In conclusion, open and real-time traffic data not only enhances residents' daily convenience but also transcends transportation, becoming a pivotal factor in numerous policy decisions that impact both society and the government(Hamilton et al., 2013). 


**References:**

Baji, P. (2018). Using Google Maps road traffic estimations to unfold spatial and temporal inequalities of urban road congestion: A pilot study from Budapest. Hungarian Geographical Bulletin, 67(1). 

Hamilton, A., Waterson, B., Cherrett, T., Robinson, A., & Snell, I. (2013). The evolution of urban traffic control: changing policy and technology. Transportation Planning and Technology, 36(1), 24-43. https://doi.org/10.1080/03081060.2012.745318 

Tseng, Y.-T., & Ferng, H.-W. (2021). An Improved Traffic Rerouting Strategy Using Real-Time Traffic Information and Decisive Weights. IEEE Transactions on Vehicular Technology, 70(10). https://doi.org/10.1109/TVT.2021.3102706 

Zafri, N. M., Afroj, S., Ali, M. A., Hasan, M. M. U., & Rahman, M. H. (2021). Effectiveness of containment strategies and local cognition to control vehicular traffic volume in Dhaka, Bangladesh during COVID-19 pandemic: Use of Google Map based real-time traffic data. PLoS one, 16(5), e0252228. 

Zubairi, J. A., Idwan, S., Haider, S. A., Hurtgen, D., Ieee 19th International Conference on Smart Communities: Improving Quality of Life Using Ict, I., & Ai Marietta, G. A. U. S. A. D. D. (2022). Smart City Traffic Management for Reducing Congestion. In 2022 IEEE 19th International Conference on Smart Communities: Improving Quality of Life Using ICT, IoT and AI (HONET) (pp. 225-230). IEEE. https://doi.org/10.1109/HONET56683.2022.10018963 


## 4. Conclusions of the goal:

In this segment, I delved into the literature to examine the current and prospective roles of traffic data in urban governance. The findings of the study underscored the pivotal role that traffic data can play as a foundation for government policy formulation. In our group project, we utilized traffic data as a key component in analyzing air pollution in Delhi. Through hands-on involvement and extensive support from additional literature, I believe I have comprehensively explored how mapping platforms have already influenced, or have the potential to influence, government decision-making processes. Consequently, I consider this learning goal successfully achieved.
