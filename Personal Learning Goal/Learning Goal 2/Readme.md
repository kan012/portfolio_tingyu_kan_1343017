# Learning Goal 2: NLP and sentiment analysis

## 1. The Background:

Initially, NLP and sentiment analysis were not included in my learning goals plan, given the initial uncertainty within the group regarding the handling of social media data during the first week of the course. However, as our group project evolved, we made the decision to explore videos and comments on TikTok related to our topic: air pollution in Delhi. Once again, I found myself in unfamiliar territory with no prior knowledge of NLP and sentiment analysis, possessing only a superficial understanding of the broader field, including machine learning and model training.


## 2. Methodology and data source used:

Conducting sentiment analysis involves two crucial steps: acquiring comments from TikTok and subsequently analyzing those comments. Since learning how to gather comments was not initially part of my plan, I sought out a tool from online sources ([Tiktok_comments_Scraper](https://www.youtube.com/watch?v=FsQEm2zalWA)) to download TikTok comments. Next, I embarked on the sentiment analysis of these comments, opting to learn the process through the [Huggingface](https://huggingface.co/blog/sentiment-analysis-python) platform — a community hub for AI, machine learning, and various datasets. On this platform, I discovered tutorials detailing the construction of a sentiment analysis model. However, attempting to develop a training model proved challenging within our limited knowledge of machine learning and natural language processing, especially in such a short period. Consequently, we opted to utilize a publicly available model on Huggingface and sought assistance from ChatGPT to generate the necessary code linking to that model.

 
## 3. Details about the implementation:

We searched the videos on Tiktok using several keywords: Delhi, air pollution, air quality. The [script](./ScrapeTikTokComments.js) obtained online enables us to download comments from the selected Tiktok videos. 

The model we used for sentiment analysis evaluates each comment on a scale of 1 to 5. 1 stands for negative sentiment, and 5 stands for positive comment. There is also another score that stands for how confident the evaluation is.

In order to make the results more clear and understandable, we also added some code about plotting the results in a bar chart and making a word clout. [Here](./Sentiment_analysis.ipynb) is the full script of sentiment analysis.


## 4. Results:

The completed results can be found [here](./Video_comments_and_sentiment_analysis). Each video's folder contains the original comments, the results of sentiment analysis, a distribution bar chart, a word cloud, and the link to the video. In conclusion, the comments from these three videos represent a more negative perspective about the air quality in Delhi.
Overall, these messages present a more negative view of the air quality in Delhi. Interestingly, many of the messages contain keywords such as carbon tax, government, etc., which we believe is a noteworthy indicator of public opinion towards the local government.


## 5. Conclusions of the goal:

Throughout my learning in NLP and sentiment analysis, I've come to the realization that while I may not have personally constructed a training model to analyze comments, I've gained a comprehensive understanding of how NLP trains imported data through models. This process has illuminated the operational principles behind sentiment analysis. Furthermore, I've grasped how entities like public relations firms and advertising agencies leverage algorithms to discern users' preferences. Despite machine learning not being an initial component of my study plan, I believe this experience has deepened my insights into the mechanics of social media.
