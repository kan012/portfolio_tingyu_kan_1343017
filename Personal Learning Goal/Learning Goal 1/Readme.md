# Learning Goal 1: Data Wrangling and API learning

## 1. The Background:

Initially, API learning wasn't part of my original set of personal learning goals, primarily because I lacked any background knowledge about APIs and hadn't even heard of them before. However, in our group project, understanding the relationship between vegetation, air quality, and temperature in Delhi necessitated the inclusion of urban traffic flow as a crucial variable. I took on the responsibility of handling traffic flow data, which could be extracted from Tomtom using the API method, contributing significantly to the overall analysis of our group project. 


## 2. Methodology and data source used:

Tomtom serves as a traffic data platform, assessing congestion levels by analyzing returned car speed data. Through its API, users gain access to real-time traffic flow updates. In the second week of the course, I acquired an understanding of what an API is and how it operates. Following this, I replicated the script provided in the course to retrieve traffic information from [Tomtom](https://developer.tomtom.com/traffic-api/api-explorer).


## 3. Details about the implementation:

The process of extracting live traffic flow data include deciding the target area(Delhi, India), converting coordinates into world map tile(z,x,y : zoom_level(z)/tile_grid(x,y)) and applying [The script](./traffic_flow_api.ipynb).

Nevertheless, the Tomtom API only provides a PNG image file devoid of detailed traffic data or an attribute table. As such, it proves unsuitable for integration with ArcGIS, the tool we utilize for overlaying and deriving analytical results from diverse datasets. Consequently, we opted for Tomtom's historical traffic report—a shapefile file encompassing essential information like vehicle speed and road class—to successfully conclude our group project. Despite being a newcomer to API learning, this experience served as a valuable exercise for me.


## 4. Results:
The [results](./results) includes three images generated from the script, containing three different zoom level of Delhi. However, these results of PNG images are not suitable for the use of ArcGIS. Instead, we directly requested the historical shapefile data to fill in the requirements of group project


## 5. Conclusions of the goal:

The original objective of this section was to wrangle open traffic data. However, as our group project unfolded, I initially leaned towards utilizing the API for acquiring real-time and accurate traffic information. Yet, when we encountered limitations with the Tomtom API's result, I adopted to a simpler alternative to ensure the seamless progress of our group project. Despite deviating from the initial data wrangling goal, this experience provided a comprehensive understanding of how API functions and the skills to retrieve information through it. In conclusion, while the task may not align precisely with data wrangling, I am pleased to have seized this opportunity to dive deeper into APIs, recognizing their significance in the field of Data Science.
