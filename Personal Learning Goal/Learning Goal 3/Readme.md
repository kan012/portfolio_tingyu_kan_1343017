# Learning Goal 3: Visualization

## 1. The Background:

Visualization is one of my initial learning goals. In order to make the information more clear and more approachable to others, I planned to learn and make an ArcGIS storymap to present my portfolio. 


## 2. Methodology and data source used:

In our group project, we applied the ArcGIS storymap to present the results. However, since I was working with social media data at the same time and Storymap is not shareable, I didn't get too involved in the actual storymap creation process.
Therefore, I'm going to use the items I was working on in the group, as well as the other sections in this portfolio, as materials for the storymap.


## 3. Details about the implementation:

In addition to the use of general pictures, maps, and other elements, I also tried to visualize some data into a 3D map, which is known as 'Scene' in ArcGIS, so that the storymap overall looked more vivid.


## 4. Results:

Click [here](https://storymaps.arcgis.com/stories/0b5decd3e62743dbb783e235f4835c0b) to see the Storymap

## 5. Conclusions of the goal:

In this project, I used ArcGIS Storymap to present the information I learned and generated in this course. I think this will be very helpful to me in the future, because in my field of study, Urban Environmental Management, besides using ArcGIS as a tool for urban planning, we often need to communicate with people from different fields, such as local governments, NGOs, and even the general public, etc. Storymap gives me a new option, and I think it is convenient and clear to use Storymap to present information such as baseline analysis, the planning process, or planning results. I am glad that I have the opportunity to learn how to visualize the information I want to present with Storymap!
