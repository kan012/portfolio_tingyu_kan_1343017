# Reflection

Collaborating with interdisciplinary team members provides us with the opportunity to share our diverse backgrounds and respective expertise, a crucial advantage in navigating the complication of an increasingly complex and interconnected future. Particularly in today's internationalized society, where challenges span various domains—social, environmental, economic, and more—solutions rarely confine themselves to narrow geographic or intellectual boundaries. Therefore, engaging in interdisciplinary, cross-disciplinary, and cross-border collaboration emerges as a pivotal factor in addressing global issues. Throughout our group project, our team comprises individuals from various fields, including Geo-information Science, Urban Environmental Management, Environmental Science, and Climate Studies. The diverse expertise within our group allows us to leverage individual strengths and exchange valuable knowledge seamlessly. For instance, when working with weather data, the team member with a background in climate studies adequately acquires and organizes pertinent information, the members from Geo-information Science experts in applying GIS tools to present our analysis processes with clarity and precision, and the Environmental Science members are able to handle and examine the statistical relationship between many layers of datasets.


An other BCC example is that when considering the handling of traffic data. Given my study on Urban Environmental Management, my enthusiasm and background knowledge extend to aspects of road planning and traffic control within urban areas, aligning with my personal learning goals. Despite having prior awareness that vehicle impacts on the urban environment encompass factors like temperature, the heat island effect, and air pollution, my understanding of the extent to which green spaces contribute to mitigation remained uncertain. Through our group project, we delved into comprehending the influence of vegetation on temperature and air pollution in the city. Collaborating with the climate-studies groupmate's analyses, our study unveiled disparities in the effectiveness of vegetation in mitigating various air pollutants—highlighting, for instance, the limited impact of green spaces on mitigating PM2.5. As the group project unfolded, I seamlessly integrated my knowledge of the urban environment into our collective efforts. Within BCC group processing, we arrived at conclusive findings that not only deepened my personal awareness but also shed light on the significant extent of vehicular impact in the urban environment.


**Things could go better**

The findings from our group project reveal that urban vegetation may not have a significant impact in mitigating air pollution and the urban heat island effect. While these results were unexpected, they prompt a thorough analysis of the aspects that went well and the factors we may have overlooked in the process.

Firstly, we acknowledge that urban air quality and temperature are influenced by numerous factors, and although our consideration of major elements such as vehicles and green spaces was essential, it proved insufficient. Factors like wind direction, population density, and transportation types, coupled with diverse forms of air pollution like PM2.5, CO2, and nitrogen emissions from vehicles, contribute significantly to the complexity. Moreover, the mitigation effectiveness of green spaces varies for different kinds of air pollutants. To streamline the process, our group project opted for a less detailed categorization in this regard. Additionally, while applying a model we deemed appropriate for clustering various factors and conducting statistical analysis, our limited experience in statistics meant that our chosen method might not have been the most optimal.


**Potential use and application of the project outcomes**

The outcomes of our group project highlighted that despite the inclusion of TikTok comments analysis, we could only obtain three videos in the end due to a limited number of related videos, and most of them lacked sufficient comments for analysis. Notably, TikTok is banned in India, and while VPN usage is widespread, it underscores the importance of acknowledging information disparities and TikTok's varying popularity among different user groups. Determining which demographics use TikTok more frequently and which groups leave comments necessitates a reliance on social science research. It's crucial to emphasize that using a TikTok comments scraper may not be legally permissible, and its use was confined to the context of this course.

Meanwhile, our findings did not reveal a significant effect of green spaces on reducing air pollution and urban temperatures. This information bears the risk of potential misuse by those advocating for removing urban green spaces, raising concerns about the implications of our outcomes.


**Reference:**

Gulikers, J., & Oonk, C. (2019). Towards a Rubric for Stimulating and Evaluating Sustainable Learning. Sustainability, 11(4), 969. https://www.mdpi.com/2071-1050/11/4/969 

