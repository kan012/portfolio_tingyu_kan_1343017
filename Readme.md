# About this Portfolio
This portfolio includes three major parts: Introduction, Personal learning goal, and Reflection

## [Introduction](./Introduction)
The introduction gives a brief summary about this portfolio and includes the [original personal learning goal](./Introduction/Individual_learning_plan_Tingyu_Kan_1343017.pdf)

## [Personal Learning Goal](./Personal Learning Goal)
This section includes four personal learning goals

1. Learning Goal 1: Data Wrangling and API learning

2. Learning Goal 2: NLP and sentiment analysis

3. Learning Goal 3: Visualization

4. Learning Goal 4: Social impact of traffic data

## [Reflection](./Reflection)

This section focuses on the reflection on learning process, things could be better as well as some reflection on group project outcomes.
