# [Introduction](./Introduction)
This section includes the [original personal learning plan](./Individual_learning_plan_Tingyu_Kan_1343017.pdf)

Within my portfolio, I've delineated four distinct learning goals. Initially, my plan encompassed only three goals. Nevertheless, through the process of our group project, we found ourselves consistently refining the necessary data parameters, eventually incorporating sentiment analysis from social media. Consequently, diving into natural language processing and sentiment analysis unexpectedly became a focal point of my learning during these couple weeks. This portfolio now encompasses four diverse learning projects, providing me with a comprehensive exploration of data science from various perspectives.
